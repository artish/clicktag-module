var ClickTag = function() {
  this.links = this.getUriParams();
};

ClickTag.prototype.getUriParams = function() {
  var query_string = {};
  var query = window.location.search.substring(1);
  var parmsArray = query.split('&');
  if(parmsArray.length <= 0) return query_string;
  for(var i = 0; i < parmsArray.length; i++) {
    var pair = parmsArray[i].split('=');
    var val = decodeURIComponent(pair[1]);
    if (val !== '' && pair[0] !== '') query_string[pair[0]] = val;
  }
  return query_string;
};

ClickTag.prototype.setLink = function(el, index) {
  if (this.links[index] !== 'undefined') {
    el.setAttribute('href', this.links[index]);
  } else {
    console.log('No such clickTag!');
  }
};

ClickTag.prototype.first = function(obj) {
  return obj[Object.keys(obj)[0]];
};

module.exports = ClickTag;