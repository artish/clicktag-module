Clicktag Module
===============

Quickly add a Clicktag to your Banner.

## Setup

To add a clicktag for your banner:

**Markup:**
```html
<a href="#clicktag" target="_blank" id="clicktag-link"></a>
```

**Javascript:**
```js
var Clicktag = require('banner-clicktag');
var clicktag = new Clicktag();

// Target Element
var clickTagEl = document.getElementById('clicktag-link');

// Setup clicktag1
clickTag.setLink(clickTagEl, 'clicktag1');
```

clicktag1 is now available at
`http://www.domain.tld/banner?clicktag1=clicktagurl.com`
